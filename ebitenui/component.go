package ebitenui

import "github.com/hajimehoshi/ebiten"

// General plan:
//   Add components to the tree
//   Render down the tree
//   Click events handler

type UI struct {
	components []compZ // z ordered components, by insert order
}

func (ui *UI) AddComponent(c Component) {
    ui.components = append(ui.components, c)
}

func (ui *UI) RemoveComponent(c Component) {
  for _, uic := range ui.components {
    if
  }
}

func (ui *UI) HandleClick(x, y int) {

}

func (ui *UI) Render(screen *ebiten.Image) error {

	for _, c := range ui.components {
		if err := c.Render(screen); err != nil {
			return err
		}
	}

	return nil
}

type Component interface {
  ID() uint64
	HandleClick(x, y int)
	Render(*ebiten.Image) error
}
