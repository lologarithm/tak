package main

import (
	"bytes"
	"flag"
	"image"
	"image/jpeg"
	"log"
	"os"
	"runtime/pprof"
	"strconv"
	"time"

	"github.com/hajimehoshi/ebiten"
	"gitlab.com/lologarithm/tak/assets/images"
	"gitlab.com/lologarithm/tak/gotak"
	"gitlab.com/lologarithm/tak/takui"
)

func profile(duration time.Duration) {
	log.Printf("Starting Profiling...")
	f, err := os.Create("tak" + strconv.Itoa(int(time.Now().Unix())) + ".pprof")
	if err != nil {
		log.Fatal("could not create CPU profile: ", err)
	}
	if err := pprof.StartCPUProfile(f); err != nil {
		log.Fatal("could not start CPU profile: ", err)
	}
	time.Sleep(duration)
	log.Printf("Stopping Profiling...")
	pprof.StopCPUProfile()
}

var runprof = flag.Bool("profile", false, "true to enable profiling")
var size = flag.Int("size", 5, "size of board")

func main() {
	flag.Parse()
	if *runprof {
		go func() {
			for {
				// Profile for 5 seconds 4 times a minute
				time.Sleep(time.Second * 15)
				profile(time.Second * 5)
			}
		}()
	}

	img, err := jpeg.Decode(bytes.NewBuffer(images.BoardIcon))
	if err != nil {
		log.Fatalf("failed to decode the board: %s", err)
	}
	ebiten.SetWindowTitle("goTak")
	ebiten.SetWindowIcon([]image.Image{img})

	if err := ebiten.Run(takui.New(gotak.NewGame(*size)).Tick, 1000, 800, 1, "goTak"); err != nil {
		log.Fatal(err)
	}
}
