
generate :
		file2byteslice --input assets/fonts/InputSans-Regular.ttf --output assets/fonts/input.go --package fonts --var InputSansReg
		file2byteslice --input assets/images/board.jpg --output assets/images/board.go --package images --var Board
		file2byteslice --input assets/images/takpiece.png --output assets/images/whitepiece.go --package images --var WhitePiece
		file2byteslice --input assets/images/takpiece2.png --output assets/images/blackpiece.go --package images --var BlackPiece
		file2byteslice --input assets/images/border.png --output assets/images/border.go --package images --var Border
		file2byteslice --input assets/images/placement.png --output assets/images/placement.go --package images --var Placement
		file2byteslice --input assets/images/wall.png --output assets/images/whitewall.go --package images --var WhiteWall
		file2byteslice --input assets/images/wall2.png --output assets/images/blackwall.go --package images --var BlackWall
		file2byteslice --input assets/images/cap.png --output assets/images/whitecap.go --package images --var WhiteCap
		file2byteslice --input assets/images/cap2.png --output assets/images/blackcap.go --package images --var BlackCap
		file2byteslice --input assets/images/roadsc.jpg --output assets/images/road.go --package images --var Road
