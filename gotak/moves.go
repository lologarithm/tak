package gotak

import (
	"errors"
	"fmt"
)

// Place a single piece at the given location.
// The piece will be at.Stack[0]
// This will follow the rules of what can be placed/moved
// but is unaware of where the piece came from. This is used to temporarily
// render movement states. To complete a full movement use 'Move'
// This is also used to place a piece from a players hand onto the board.
func Place(b *Board, at Space) (*Board, error) {
	target := b.Spaces[at.X][at.Y]

	newB := CloneBoard(b)
	if len(target.Stack) > 0 {
		if target.Stack[0].Type == PieceWall {
			if at.Stack[0].Type != PieceCapstone {
				return b, errors.New("unable to place on a wall")
			}
			// Walls get converted to stones when crushed.
			newB.Spaces[at.X][at.Y].Stack[0].Type = PieceStone
		}
	}
	newB.Spaces[at.X][at.Y].Stack = append([]Piece{at.Stack[0]}, newB.Spaces[at.X][at.Y].Stack...)
	return newB, nil
}

// Move pieces from -> to and returns a new copy of the board
// Will do full movement validation. This is used to complete a movement.
// Uses from.Stack to determine what was removed from the source.
// to.Stack is what was placed down in each space.
// UI should prevent incorrect moves but we can return errors here just in case
// there was an error in programming logic. The errors mostly just exist for
// developers to debug what went wrong.
func Move(b *Board, from Space, to []Space) (*Board, error) {
	if len(to) == 0 {
		return b, errors.New("no destination")
	}
	source := b.Spaces[from.X][from.Y]
	count := len(from.Stack)
	if len(source.Stack) < count || count > b.Size {
		return b, errors.New("too many pieces attempted to move")
	}
	for i := 0; i < count; i++ {
		if from.Stack[i] != source.Stack[i] {
			return b, errors.New(fmt.Sprintf("invalid piece in move: From: %#v, Source: %#v", from, source))
		}
	}
	horiz := true // decide if this is horizontal or vertical movement.
	if from.Y != to[0].Y {
		horiz = false
	}
	prev := source
	for i := 0; i < len(to); i++ {
		if horiz {
			if to[i].Y != prev.Y {
				return b, errors.New("invalid move, not a straight line.")
			}
		} else {
			if to[i].X != prev.X {
				return b, errors.New("invalid move, not a straight line.")
			}
		}
	}
	// TODO: Validate that pieces can be placed and that its in a straight line.

	// Move is valid -- now move all the pieces where they go.
	newB := CloneBoard(b)
	// Remove the pieces from the source stack
	newB.Spaces[from.X][from.Y].Stack = newB.Spaces[from.X][from.Y].Stack[count:]

	return newB, nil
}
