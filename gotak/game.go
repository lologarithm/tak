package gotak

func NewGame(size int) *Game {
	board := NewBoard(size)
	game := &Game{
		Board: board,
		Turn:  SideWhite,
	}
	return game
}

type Game struct {
	Board *Board // Current state of the board.

	// Current Player Status
	WhiteStash    int
	WhiteCapStash int
	BlackStash    int
	BlackCapStash int

	Round int // round number
	Turn  Side

	// If populated the game is done.
	Done []Space
}
