package gotak

import (
	"fmt"
	"testing"
)

func TestFindRoad(t *testing.T) {
	b := NewBoard(4)

	for x := range b.Spaces {
		b.Spaces[x][1].Stack = []Piece{NewPiece(SideWhite, PieceStone)}
	}

	road := FindRoad(b)

	if len(road) == 0 {
		t.FailNow()
	}

	fmt.Printf("Found Road: \n")
	for _, s := range road {
		fmt.Printf(" (%d,%d)\n", s.X, s.Y)
	}
}

func TestBoardString(t *testing.T) {
	b := NewBoard(5)

	for x := range b.Spaces {
		b.Spaces[x][1].Stack = []Piece{NewPiece(SideWhite, PieceStone)}
		b.Spaces[x][3].Stack = []Piece{NewPiece(SideBlack, PieceStone)}
	}

	fmt.Printf("%s\n", b.String())
}
