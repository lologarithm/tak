package gotak

import "strings"

func NewBoard(size int) *Board {
	b := &Board{
		Size: size,
	}
	spaces := make([][]Space, size)
	for x := range spaces {
		spaces[x] = make([]Space, size)
		for y := range spaces[x] {
			spaces[x][y].X = x
			spaces[x][y].Y = y
		}
	}
	b.Spaces = spaces
	return b
}

func CloneBoard(b *Board) *Board {
	newB := Board{
		Size:   b.Size,
		Spaces: make([][]Space, b.Size),
	}
	for i, col := range b.Spaces {
		newCol := make([]Space, b.Size)
		copy(newCol, col)
		newB.Spaces[i] = newCol
	}
	return &newB
}

func NewSpace(x, y int) Space {
	return Space{
		X: x,
		Y: y,
	}
}

func NewPiece(s Side, t PieceType) Piece {
	return Piece{Side: s, Type: t}
}

type Board struct {
	Size   int       // Size of the board (num spaces per side)
	Spaces [][]Space // X/Y
}

func (b Board) String() string {
	singleCol := " │"
	//singleRow := "─"
	// open := "┼"
	// white := "○"
	// black := "●"
	// ▶ ▷
	// ■ □
	rows := make([]string, b.Size*3)
	rows[0] = strings.Repeat(singleCol, b.Size)
	for i := 0; i < b.Size-1; i++ {
		rows[i*3+2] = strings.Repeat(singleCol, b.Size)
		rows[i*3+3] = strings.Repeat(singleCol, b.Size)
	}
	for _, col := range b.Spaces {
		for n, s := range col {
			if len(s.Stack) == 0 {
				rows[n*3+1] += "─┼"
			} else if s.Stack[0].Side == SideWhite {
				rows[n*3+1] += "─○"
			} else {
				rows[n*3+1] += "─●"
			}
		}
		// rows = append(rows, strings.Repeat(singleCol, len(b.Spaces)))
	}
	for i := 0; i < b.Size; i++ {
		rows[i*3+1] += "-"
	}
	rows[b.Size*3-1] = strings.Repeat(singleCol, len(b.Spaces))
	return strings.Join(rows, "\n")
}

type Space struct {
	X     int
	Y     int
	Stack []Piece
}

type Piece struct {
	Side Side
	Type PieceType
}

type Side int

func (s Side) Swap() Side {
	if s == SideBlack {
		return SideWhite
	}
	return SideBlack
}

type PieceType int

// Side and piece type constants
const (
	SideWhite Side = 1
	SideBlack Side = 2

	PieceStone    PieceType = 1
	PieceWall     PieceType = 2
	PieceCapstone PieceType = 3
)

func (s Side) String() string {
	if s == SideWhite {
		return "white"
	}
	return "black"
}

// ValidMoves searches the board for valid moves
// starting at the given space.
func ValidMoves(b *Board, s Space) []Space {
	if len(s.Stack) == 0 {
		return []Space{}
	}

	return []Space{}
}

// FindRoad searches all edges of the board for a road.
func FindRoad(b *Board) []Space {
	// Check horizontal
	for x := 0; x < b.Size; x++ {
		if road := findRoadFrom(b, x, 0); len(road) > 0 {
			return road
		}
	}
	// Check vertical
	for y := 0; y < b.Size; y++ {
		if road := findRoadFrom(b, 0, y); len(road) > 0 {
			return road
		}
	}
	return []Space{}
}

// findRoadFrom will find a road from the given start point.
// Must be an edge of the board. (This is called from the public FindRoad)
// Returns empty space list if there is no road.
func findRoadFrom(b *Board, x, y int) []Space {
	stack := b.Spaces[x][y].Stack
	if len(stack) == 0 {
		return []Space{}
	}

	victoryX := -1
	victoryY := -1
	if x == 0 {
		victoryX = b.Size - 1
	} else if x == b.Size-1 {
		victoryX = 0
	}
	if y == 0 {
		victoryY = b.Size - 1
	} else if y == b.Size-1 {
		victoryY = 0
	}
	// Now travel!
	return findNextRoadStone(b, stack[0].Side, x, y, victoryX, victoryY, []Space{})
}

// findNextRoadStone will find a valid path to the given edge (either victoryX or Y).
func findNextRoadStone(b *Board, c Side, x, y int, victoryX, victoryY int, seen []Space) []Space {
	stack := b.Spaces[x][y].Stack
	// No piece, no road
	if len(stack) == 0 {
		return []Space{}
	}
	for i := range seen {
		if seen[i].X == x && seen[i].Y == y {
			return []Space{}
		}
	}
	// Wrong color? no road
	if stack[0].Side != c {
		return []Space{}
	}
	// Wall piece? No road
	if stack[0].Type == PieceWall {
		return []Space{}
	}
	// At victory edge? yay! return this piece
	if x == victoryX || y == victoryY {
		return []Space{b.Spaces[x][y]}
	}
	seen = append(seen, b.Spaces[x][y])
	if x > 0 {
		spaces := findNextRoadStone(b, c, x-1, y, victoryX, victoryY, seen)
		if len(spaces) > 0 {
			// Found a road from here, add ourselves and return
			finalSpace := b.Spaces[x][y]
			return append([]Space{finalSpace}, spaces...)
		}
	}
	if x < b.Size-1 {
		spaces := findNextRoadStone(b, c, x+1, y, victoryX, victoryY, seen)
		if len(spaces) > 0 {
			// Found a road from here, add ourselves and return
			finalSpace := b.Spaces[x][y]
			return append([]Space{finalSpace}, spaces...)
		}
	}
	if y > 0 {
		spaces := findNextRoadStone(b, c, x, y-1, victoryX, victoryY, seen)
		if len(spaces) > 0 {
			// Found a road from here, add ourselves and return
			finalSpace := b.Spaces[x][y]
			return append([]Space{finalSpace}, spaces...)
		}
	}
	if y < b.Size-1 {
		spaces := findNextRoadStone(b, c, x, y+1, victoryX, victoryY, seen)
		if len(spaces) > 0 {
			// Found a road from here, add ourselves and return
			finalSpace := b.Spaces[x][y]
			return append([]Space{finalSpace}, spaces...)
		}
	}

	return []Space{}
}
