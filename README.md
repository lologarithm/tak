# goTak
Go implementation of [Tak](https://cheapass.com/free-games/tak/) the board game

Building on windows (taken from ebiten):
env CGO_ENABLED=1 CXX=x86_64-w64-mingw32-g++ CC=x86_64-w64-mingw32-gcc GOOS=windows GOARCH=amd64 go build ./cmd/tak/
## Big Status
Basic client mostly functional for local versus play.
Server is non-existent so multiplayer is completely unsupported at this point.

## Feature Status
* Basic Features
  * First turn players swap placement
  * Very little game validation yet
    * Need to validate movement still
    * No validation of placement (players can go over placement limits)
    * No automatic check for full board
  * Need to add touch support and get it compiling on mobile
  * Add some keyboard handlers because its annoying ESC doesn't cancel placement.
    * Perhaps also allow 'esc' to cancel movement as well
  * Allow right click to un-do placements in a line. Only commit the action once all pieces have been placed.
  * Game Menu
    * Player Stashes
    * Turn Indicator
* Local VS
  * Basic game play functional
* Single Player needs to be implemented
  * AI needs to be implemented
* Multiplayer
  * Need a server
* Support Portable Tak Notation - [PTN](https://www.reddit.com/r/Tak/wiki/portable_tak_notation)
