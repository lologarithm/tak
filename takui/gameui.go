package takui

import (
	"log"
	"strconv"
	"time"

	"github.com/hajimehoshi/ebiten"
	"github.com/hajimehoshi/ebiten/ebitenutil"
	"gitlab.com/lologarithm/tak/gotak"
)

func New(g *gotak.Game) *GameUI {
	return &GameUI{
		game:  g,
		input: &input{},
	}
}

type GameUI struct {
	game *gotak.Game

	// Interaction state, includes the current pieces the player has in 'hand'
	// PlacementMenu is used for placing new pieces on the board.
	input   *input
	hand    hand
	placing *PlacementMenu

	Profiling bool
}

type hand struct {
	held      []gotak.Piece
	placed    []gotak.Space // List of destinations and which pieces went to each
	origin    gotak.Space
	lastPlace gotak.Space
	source    *gotak.Board // board state before hand hold
}

var (
	frameTimes []time.Duration
	frame      int
	lastStart  time.Time
)

func (g *GameUI) Tick(screen *ebiten.Image) error {
	// Profile our application
	frameTimes = append(frameTimes, time.Since(lastStart))
	if len(frameTimes) > 30 {
		diff := len(frameTimes) - 30
		frameTimes = frameTimes[diff:]
	}
	lastStart = time.Now()

	// First do an update to handle input and update models.
	if err := g.Update(); err != nil {
		return err
	}
	if ebiten.IsDrawingSkipped() {
		return nil
	}

	// Then actually render!
	// TODO: replace render with generic component rendering system!
	g.Render(screen)

	// Only print FPS with running profiling
	if g.Profiling {
		// Print the time between frames
		var total time.Duration
		for _, v := range frameTimes {
			total += v / time.Millisecond
		}
		fps := float64(total) / float64(len(frameTimes))
		ebitenutil.DebugPrint(screen, strconv.FormatFloat(1000.0/fps, 'f', 2, 64))
	}

	frame++
	return nil
}

// Render kicks of a render cycle
func (g *GameUI) Render(screen *ebiten.Image) {
	DrawBoard(screen, g.game.Board, g.game.Done)
	DrawInteraction(screen, g)
}

func (g *GameUI) handleStartPlace(x, y int) {
	if g.placing == nil {
		g.placing = &PlacementMenu{
			X:        x,
			Y:        y,
			Turn:     g.game.Turn,
			Complete: func(pt gotak.PieceType) { g.handleFinishPlace(x, y, pt) },
		}
	}
}

func (g *GameUI) handleFinishPlace(x, y int, pt gotak.PieceType) {
	g.placing = nil
	// We already checked if the spot is empty.
	newB, err := gotak.Place(g.game.Board, gotak.Space{
		X:     x,
		Y:     y,
		Stack: []gotak.Piece{{Type: pt, Side: g.game.Turn}},
	})
	if err != nil {
		log.Printf("Failed to place piece: %s", err)
		return
	}

	g.game.Board = newB
	if g.game.Turn == gotak.SideWhite {
		g.game.WhiteStash--
		g.game.Turn = gotak.SideBlack
	} else {
		g.game.BlackStash--
		g.game.Turn = gotak.SideWhite
	}
}

func (g *GameUI) handleLeftClick(e event) {
	// Check if we have a placement menu active.
	if g.placing != nil {
		if handled := g.placing.HandleClick(e.x, e.y); !handled {
			g.placing = nil // clear menu
		}
		return
	}

	// Otherwise check for a click inside the board
	if e.x/100 >= g.game.Board.Size || e.y/100 >= g.game.Board.Size {
		return
	}
	space := g.game.Board.Spaces[e.x/100][e.y/100]
	handSize := len(g.hand.held)

	if handSize == 0 {
		if len(space.Stack) == 0 {
			g.handleStartPlace(space.X, space.Y)
		} else if space.Stack[0].Side == g.game.Turn {
			// This is always a pickup
			g.hand.source = g.game.Board
			g.hand.origin = space
			g.hand.lastPlace = space
			g.hand.held = append(g.hand.held, space.Stack[0])

			g.game.Board = gotak.CloneBoard(g.game.Board)
			g.game.Board.Spaces[space.X][space.Y].Stack = space.Stack[1:]
		}
	} else {
		// This is either a pickup or a place
		if space.X == g.hand.origin.X && space.Y == g.hand.origin.Y {
			if len(space.Stack) == 0 || len(g.hand.held) == g.game.Board.Size {
				// Can't pickup more if there is nothing to grab.
				// Also can't pickup more than the size of the board.
				return
			}
			// Pickup
			g.hand.held = append(g.hand.held, space.Stack[0])
			g.game.Board = gotak.CloneBoard(g.game.Board)
			g.game.Board.Spaces[space.X][space.Y].Stack = space.Stack[1:]
		} else {
			xdiff := absInt(space.X - g.hand.lastPlace.X)
			ydiff := absInt(space.Y - g.hand.lastPlace.Y)
			if xdiff+ydiff > 1 {
				return // can't move more than 1 space
			}

			// TODO: make sure we are moving in a stright line.
			at := space
			at.Stack = []gotak.Piece{g.hand.held[handSize-1]}
			newb, err := gotak.Place(g.game.Board, at)
			if err != nil {
				log.Printf("Can't place: %s", err)
				// Invalid place, ignore and continue
				return
			}
			g.hand.held = g.hand.held[:handSize-1]
			g.hand.lastPlace = newb.Spaces[at.X][at.Y]

			g.game.Board = newb
			if len(g.hand.held) == 0 {
				g.game.Turn = g.game.Turn.Swap()
			}
		}
	}
}

func (g *GameUI) handleRightClick(e event) {
	handSize := len(g.hand.held)
	if handSize == 0 {
		return
	}
	// Validate click is inside the board area first.
	if e.x/100 >= g.game.Board.Size || e.y/100 >= g.game.Board.Size {
		return
	}
	space := g.game.Board.Spaces[e.x/100][e.y/100]
	if space.X == g.hand.origin.X && space.Y == g.hand.origin.Y &&
		g.hand.lastPlace.X == space.X && g.hand.lastPlace.Y == space.Y {
		at := space
		at.Stack = []gotak.Piece{g.hand.held[handSize-1]}

		newb, err := gotak.Place(g.game.Board, at)
		if err != nil {
			log.Printf("Invalid right click placement (%#v): %s", e.t, err)
			return // probably shouldn't happen
		}
		g.hand.held = g.hand.held[:handSize-1]
		g.hand.lastPlace = newb.Spaces[at.X][at.Y]
		g.game.Board = newb
	}
}

// update will process events and update the board.
// returns true if the game is complete.
func (g *GameUI) update() {
	// Handle interactions first
	for _, e := range g.input.events {
		if g.placing != nil {
			// Only interactions are either closing menu or interacting with menu.
		}

		if e.t == eventTypeLeftClick {
			g.handleLeftClick(e)
		}

		if e.t == eventTypeRightClick {
			g.handleRightClick(e)
		}
	}

	g.input.events = g.input.events[:0]
	if len(g.hand.held) == 0 {
		// Don't check for end until player is done
		g.game.Done = gotak.FindRoad(g.game.Board)
	}
}

func absInt(v int) int {
	if v < 0 {
		return -v
	}
	return v
}

func (g *GameUI) Update() error {
	if len(g.game.Done) > 0 {
		return nil
	}
	g.input.Update()
	g.update()

	// This line is only hit once because we dont update after game is done.
	if len(g.game.Done) > 0 {
		log.Printf("Game done, winner: %s", g.game.Done[0].Stack[0].Side)
	}
	return nil
}

// TODO: Input will maybe manage menus/UI interactions
// I don't like that rendering and interacting are so separated right now.
// Maybe moving some of that together so that its handled in the same place could be a good idea.
//  Then the input system would convert from 'ui events' to 'player intent' via a game interaction api
type input struct {
	leftDown bool
	leftTime time.Time // Used for long click/press

	rightDown bool

	mousePos event
	// Click Info
	events []event
}

type event struct {
	x int
	y int
	t int
}

const (
	eventTypeLeftClick  = 1
	eventTypeRightClick = 2
)

func (i *input) Update() {
	x, y := ebiten.CursorPosition()

	leftPressed := ebiten.IsMouseButtonPressed(ebiten.MouseButtonLeft)
	if i.leftDown && !leftPressed {
		// Left Mouse Up
		i.events = append(i.events, event{x: x, y: y, t: eventTypeLeftClick})
		i.leftDown = false
	} else if leftPressed {
		i.leftDown = true
		i.leftTime = time.Now()
	}

	rightPressed := ebiten.IsMouseButtonPressed(ebiten.MouseButtonRight)
	if i.rightDown && !rightPressed {
		// Right Mouse Up
		i.events = append(i.events, event{x: x, y: y, t: eventTypeRightClick})
		i.rightDown = false
	} else if rightPressed {
		i.rightDown = true
	}

	i.mousePos.x = x
	i.mousePos.y = y

	// TODO: ebiten.TouchIDs()
}
