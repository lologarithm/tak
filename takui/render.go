package takui

import (
	"bytes"
	"fmt"
	"image"
	"image/color"
	"image/jpeg"
	"image/png"
	"io"
	"log"
	"math"
	"os"

	"github.com/hajimehoshi/ebiten"
	"gitlab.com/lologarithm/tak/assets/images"
	"gitlab.com/lologarithm/tak/gotak"
	"golang.org/x/image/font"
)

var blackPieceImg *ebiten.Image
var whitePieceImg *ebiten.Image
var blackWallImg *ebiten.Image
var whiteWallImg *ebiten.Image
var blackCapImg *ebiten.Image
var whiteCapImg *ebiten.Image

var boardImg *ebiten.Image
var borderImg *ebiten.Image
var placementImg *ebiten.Image
var roadImg *ebiten.Image

var highlight *ebiten.Image

var inputFont font.Face

type decoder func(io.Reader) (image.Image, error)

func loadImg(d decoder, data []byte) *ebiten.Image {
	img, err := d(bytes.NewBuffer(data))
	if err != nil {
		log.Fatalf("failed to decode image: %s", err)
	}
	eimg, err := ebiten.NewImageFromImage(img, ebiten.FilterDefault)
	if err != nil {
		log.Fatalf("Failed to create ebiten Image: %s", err)
	}
	return eimg
}

func init() {
	boardImg = loadImg(jpeg.Decode, images.Board)
	whitePieceImg = loadImg(png.Decode, images.WhitePiece)
	blackPieceImg = loadImg(png.Decode, images.BlackPiece)
	whiteWallImg = loadImg(png.Decode, images.WhiteWall)
	blackWallImg = loadImg(png.Decode, images.BlackWall)
	whiteCapImg = loadImg(png.Decode, images.WhiteCap)
	blackCapImg = loadImg(png.Decode, images.BlackCap)

	borderImg = loadImg(png.Decode, images.Border)
	placementImg = loadImg(png.Decode, images.Placement)
	roadImg = loadImg(jpeg.Decode, images.Road)

	// tt, err := truetype.Parse(fonts.InputSansReg)
	// if err != nil {
	// 	log.Fatal(err)
	// }
	// const dpi = 72
	// inputFont = truetype.NewFace(tt, &truetype.Options{
	// 	Size:    24,
	// 	DPI:     dpi,
	// 	Hinting: font.HintingFull,
	// })

	var err error
	highlight, err = ebiten.NewImage(spaceWidth, spaceWidth, ebiten.FilterDefault)
	if err != nil {
		fmt.Printf("Failed load highlight: %s", err)
		os.Exit(1)
	}
	highlight.Fill(color.RGBA{0, 200, 50, 20})
}

func DrawRoad(screen *ebiten.Image, road []gotak.Space) {
	col := &ebiten.ColorM{}
	// col.Scale(1, 1, 1, 1)
	for _, sp := range road {
		geom := &ebiten.GeoM{}
		geom.Scale(0.5, 0.5)
		geom.Translate(float64(sp.X)*spaceWidth, float64(sp.Y)*spaceWidth)

		screen.DrawImage(roadImg, &ebiten.DrawImageOptions{
			GeoM:   *geom,
			ColorM: *col,
		})
	}
}

func drawPossMove(screen *ebiten.Image, x, y int) {
	geom := &ebiten.GeoM{}
	geom.Translate(float64(x)*spaceWidth, float64(y)*spaceWidth)
	screen.DrawImage(highlight, &ebiten.DrawImageOptions{
		GeoM: *geom,
	})
}

func DrawInteraction(screen *ebiten.Image, g *GameUI) {
	if g.placing != nil {
		RenderPlacementMenu(screen, g.placing)
		return
	}
	if len(g.hand.held) > 0 {
		// Draw in-hand pieces @cursor
		drawStack(screen, g.hand.held, 0.25, float64(g.input.mousePos.x)-10, float64(g.input.mousePos.y)-10)

		// TODO: actually use game logic to decide which spaces are actually valid (like when moving multiple spaces)
		// currently we only highlight around the source of the stones
		// sx := g.hand.origin.X
		// sy := g.hand.origin.Y
		//
		// if sx < g.game.Board.Size-1 {
		// 	drawPossMove(screen, sx+1, sy)
		// }
		// if sx > 0 {
		// 	drawPossMove(screen, sx-1, sy)
		// }
		// if sy < g.game.Board.Size-1 {
		// 	drawPossMove(screen, sx, sy+1)
		// }
		// if sy > 0 {
		// 	drawPossMove(screen, sx, sy-1)
		// }
	} else {
		sx := g.input.mousePos.x / 100
		sy := g.input.mousePos.y / 100
		if sx > -1 && sx < g.game.Board.Size && sy > -1 && sy < g.game.Board.Size {
			if len(g.game.Board.Spaces[sx][sy].Stack) == 0 {
				geom := &ebiten.GeoM{}
				geom.Scale(0.9, 0.9)
				geom.Translate(float64(sx)*spaceWidth+5, float64(sy)*spaceWidth+2)
				screen.DrawImage(placementImg, &ebiten.DrawImageOptions{
					GeoM: *geom,
				})
			} else if g.game.Board.Spaces[sx][sy].Stack[0].Side == g.game.Turn {
				geom := &ebiten.GeoM{}
				geom.Translate(float64(sx)*spaceWidth, float64(sy)*spaceWidth)
				screen.DrawImage(highlight, &ebiten.DrawImageOptions{
					GeoM: *geom,
				})
			} else {
				geom := &ebiten.GeoM{}
				geom.Translate(float64(sx)*spaceWidth, float64(sy)*spaceWidth)
				cm := &ebiten.ColorM{}
				cm.ChangeHSV(math.Pi, 1, 1)
				screen.DrawImage(highlight, &ebiten.DrawImageOptions{
					ColorM: *cm,
					GeoM:   *geom,
				})
			}
		}
	}
}

func DrawUI(screen *ebiten.Image) {
	// ebiten.DrawText()
	// screen.
	//	Needs:
	//		Current Turn Indicator
	//		Player Stashes

	//	Later:
	//		Game Menu? (Single/Multiplayer, Board Size)
	//
}

func DrawBoard(screen *ebiten.Image, b *gotak.Board, road []gotak.Space) {
	geom := &ebiten.GeoM{}
	sx, sy := screen.Size()
	bx, by := boardImg.Size()

	geom.Scale(float64(sx)/float64(bx), float64(sy)/float64(by))
	screen.DrawImage(boardImg, &ebiten.DrawImageOptions{
		GeoM: *geom,
	})

	// end := float64(b.Size) * spaceWidth
	for i := 0; i < b.Size+1; i++ {
		geom := &ebiten.GeoM{}
		geom.Scale(1.4, float64(b.Size))
		geom.Translate(float64(i)*spaceWidth-3, 0)
		if i == 0 {
			geom.Translate(2, 0)
		}
		screen.DrawImage(borderImg, &ebiten.DrawImageOptions{
			GeoM: *geom,
		})

		geom = &ebiten.GeoM{}
		geom.Scale(1.4, float64(b.Size))
		geom.Rotate(-math.Pi / 2)
		geom.Translate(0, float64(i)*spaceWidth)
		if i == 0 {
			geom.Translate(0, 2)
		}
		screen.DrawImage(borderImg, &ebiten.DrawImageOptions{
			GeoM: *geom,
		})
	}

	// If game complete. render the road.
	if len(road) > 0 {
		// Render road overlay
		DrawRoad(screen, road)
	}

	for _, list := range b.Spaces {
		for _, sp := range list {
			if len(sp.Stack) == 0 {
				continue
			}
			drawStack(screen, sp.Stack, 0.4, float64(sp.X)*spaceWidth, float64(sp.Y)*spaceWidth)
		}
	}
}

const spaceWidth = 100.0
const pieceWidth = 200.0
const pieceHeight = 220.0

func spaceOffset(stackSize int, scale float64) float64 {
	return (spaceWidth-(pieceWidth*scale))/2.0 + float64(stackSize-1)*(8.0*scale)
}

func spaceOffsetY(stackSize int, scale float64) float64 {
	return (spaceWidth-(pieceHeight*scale))/2.0 + float64(stackSize-1)*(8.0*scale) - 2
}

func drawStack(screen *ebiten.Image, stack []gotak.Piece, scale float64, x, y float64) {
	if len(stack) > 5 {
		scale *= .75
	}
	if len(stack) > 8 {
		scale *= .8
	}
	edge := spaceOffset(len(stack), scale)
	edgeY := spaceOffsetY(len(stack), scale)
	for i := len(stack) - 1; i > -1; i-- {
		drawPiece(screen, imageForPiece(stack[i]), scale, x+edge, y+edgeY)
		edge -= 16 * scale
		edgeY -= 16 * scale
	}
}

func imageForPiece(p gotak.Piece) *ebiten.Image {
	if p.Type == gotak.PieceStone {
		if p.Side == gotak.SideBlack {
			return blackPieceImg
		} else {
			return whitePieceImg
		}
	}

	if p.Type == gotak.PieceWall {
		if p.Side == gotak.SideBlack {
			return blackWallImg
		} else {
			return whiteWallImg
		}
	}

	if p.Type == gotak.PieceCapstone {
		if p.Side == gotak.SideBlack {
			return blackCapImg
		}
		return whiteCapImg
	}

	// Probably should return a fallback in case we ever mess up a model.
	// But in general this should never happen.
	return nil
}
func drawPiece(screen *ebiten.Image, piece *ebiten.Image, scale float64, x float64, y float64) {
	geom := &ebiten.GeoM{}
	geom.Scale(scale, scale)
	geom.Translate(x, y)
	screen.DrawImage(piece, &ebiten.DrawImageOptions{
		GeoM: *geom,
	})
}
