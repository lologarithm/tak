package takui

import (
	"image/color"

	"github.com/hajimehoshi/ebiten"
	"github.com/hajimehoshi/ebiten/ebitenutil"
	"gitlab.com/lologarithm/tak/gotak"
)

type PlaceComplete func(gotak.PieceType)

type PlacementMenu struct {
	X, Y     int // board location to paint menu
	Turn     gotak.Side
	Complete PlaceComplete // Complete is called when user clicks on item

	frame int // internal animation frame
}

func intmin(a, b int) int {
	if a < b {
		return a
	}
	return b
}

func RenderPlacementMenu(screen *ebiten.Image, pm *PlacementMenu) {
	menuframes := 5
	submenuframes := 5
	submenustart := menuframes / 2

	shrinkage := spaceWidth * 0.1
	stepFrame := float64(intmin(pm.frame, menuframes))

	ebitenutil.DrawRect(screen, float64(pm.X)*spaceWidth+shrinkage/2, float64(pm.Y)*spaceWidth+shrinkage/2, spaceWidth-shrinkage, spaceWidth-shrinkage, color.RGBA{0xff, 0xff, 0xff, 0x40})

	scale := (0.2 * stepFrame) / 2.2

	x := float64(pm.X)*spaceWidth + shrinkage/2 + stepFrame*5
	y := float64(pm.Y)*spaceWidth + shrinkage/2 + stepFrame*5 - 10
	drawPiece(screen, imageForPiece(gotak.Piece{Type: gotak.PieceStone, Side: pm.Turn}), scale, x, y)

	if pm.frame >= submenustart {
		scale = (0.1 * stepFrame) / 2.2
		stepFrame = float64(intmin(pm.frame-submenustart, submenuframes))
		x := (float64(pm.X)+0.5)*spaceWidth + shrinkage/2 + stepFrame*((pieceWidth*scale)/float64(submenuframes))
		y := float64(pm.Y)*spaceWidth + shrinkage/2 - 3
		// w := spaceWidth/4 + stepFrame*(spaceWidth/(float64(submenuframes)*4))
		// h := w
		// ebitenutil.DrawRect(screen, x, y, w, h, color.RGBA{0xff, 0xff, 0xff, 0x80})
		drawPiece(screen, imageForPiece(gotak.Piece{Type: gotak.PieceWall, Side: pm.Turn}), scale, x, y)

		x = (float64(pm.X))*spaceWidth + shrinkage/2
		y = (float64(pm.Y)+0.5)*spaceWidth + shrinkage/2 - 3 + stepFrame*((pieceWidth*scale)/float64(submenuframes))
		// ebitenutil.DrawRect(screen, x, y, w, h, color.RGBA{0xff, 0xff, 0xff, 0x80})
		drawPiece(screen, imageForPiece(gotak.Piece{Type: gotak.PieceCapstone, Side: pm.Turn}), scale, x, y)
	}
	pm.frame = pm.frame + 1
}

// HandleClick returns true if handle was inside the bounds.
func (pm *PlacementMenu) HandleClick(x, y int) bool {
	left := int((float64(pm.X) + 0.25) * spaceWidth)
	right := left + spaceWidth
	top := int((float64(pm.Y) + 0.25) * spaceWidth)
	bottom := top + spaceWidth
	if x < right && x > left && y < bottom && y > top {
		pm.Complete(gotak.PieceStone)
		return true
	}

	left = int((float64(pm.X) + 1) * spaceWidth)
	right = left + spaceWidth/2
	top = int((float64(pm.Y)) * spaceWidth)
	bottom = top + spaceWidth/2
	if x < right && x > left && y < bottom && y > top {
		pm.Complete(gotak.PieceWall)
		return true
	}

	left = int((float64(pm.X)) * spaceWidth)
	right = left + spaceWidth/2
	top = int((float64(pm.Y) + 1) * spaceWidth)
	bottom = top + spaceWidth/2
	if x < right && x > left && y < bottom && y > top {
		pm.Complete(gotak.PieceCapstone)
		return true
	}

	return false
}
